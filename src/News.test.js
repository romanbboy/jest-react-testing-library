import { render, act } from '@testing-library/react'
import userEvent from "@testing-library/user-event"
import axios from "axios";
import News from './News';

jest.mock('axios');
const hits = [
  { objectID: "1", title: "Angular" },
  { objectID: "2", title: "React" }
]

describe("News", () => {
  it("Fetches news from API", async () => {
    axios.get.mockImplementationOnce(() => Promise.resolve({ data: { hits } }))
    const {getByRole, findAllByRole} = render(<News />);
    userEvent.click(getByRole('button'));
    const items = await findAllByRole('listitem');
    expect(items).toHaveLength(2)
    //
    expect(axios.get).toHaveBeenCalledTimes(1);
    expect(axios.get).toHaveBeenCalledWith(
      "http://hn.algolia.com/api/v1/search?query=React"
    );
  })

  it("Fetches news from API with reject", async () => {
    axios.get.mockImplementationOnce(() => Promise.reject(new Error()))
    const {getByRole, findByText} = render(<News />);
    userEvent.click(getByRole('button'));
    const msg = await findByText(/Something went wrong .../);
    expect(msg).toBeInTheDocument();
  })

  it("fetches news from an API (alternative)", async () => {
    const promise = Promise.resolve({ data: { hits } });
    axios.get.mockImplementationOnce(() => promise);
    const { getByRole, getAllByRole } = render(<News />);
    userEvent.click(getByRole("button"));
    await act(() => promise);
    expect(getAllByRole("listitem")).toHaveLength(2);
  });
})