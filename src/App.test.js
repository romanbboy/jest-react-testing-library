import { render, screen, fireEvent } from '@testing-library/react';
import userEvent from "@testing-library/user-event"
import App from './App';

describe("App", () => {
  // Нужно найти элемент - используем getBy
  // Нужно показать что элемента нет в DOM - используем queryBy
  // Хотим сказать, что элемента нету но при работе ассихронного кода он появится то - findBy
  // Если элементов несколько то - allBy

  it("renders77 App component", async () => {
    render(<App />);
    expect(screen.getByText(/Search:/i)).toBeInTheDocument()
    expect(screen.getByRole('textbox')).toBeInTheDocument()

    // если утверждаем что в разметке чего то НЕТ, то используем queryBy
    expect(screen.queryByText(/Searches for React/i)).toBeNull()

    // используем для ассинхронных операций, когда потом что то появляется то - findBy
    expect(await screen.findByText(/It is: Roman/i)).toBeInTheDocument();

    expect(screen.getByLabelText(/search/i)).not.toBeRequired()
  })

  it("fire event", async () => {
    // тут мы будем имитировать типа как то взаимодействуем с компонентом (поле воода, нажатие кнопки и тд)
    render(<App />);
    await screen.findByText(/It is: Roman/i);
    expect(screen.queryByText(/Searches for Vue/)).toBeNull();

    /*fireEvent.change(screen.getByRole('textbox'), {
      target: {value: 'Vue'}
    })*/
    userEvent.type(screen.getByRole('textbox'), "Vue")
    expect(screen.queryByText(/Searches for Vue/)).toBeInTheDocument();

  })
});

describe('events', () => {
  it('checkbox click', () => {
    const handleChange = jest.fn();
    const {container} = render(<input type="checkbox" onChange={handleChange} />)
    const checkbox = container.firstChild // или можно screen.getByRole
    expect(checkbox).not.toBeChecked();
    // fireEvent.click(checkbox);
    userEvent.click(checkbox);
    expect(handleChange).toHaveBeenCalledTimes(1); // типа после клика ВЫШЕ у нас функция вызвалась один раз
    expect(checkbox).toBeChecked();
  })

  it('double click', () => {
    const onChange = jest.fn();
    const {container} = render(<input type="checkbox" onChange={onChange} />)
    const checkbox = container.firstChild // или можно screen.getByRole
    expect(checkbox).not.toBeChecked();
    userEvent.dblClick(checkbox);
    expect(onChange).toHaveBeenCalledTimes(2); // типа после клика ВЫШЕ у нас функция вызвалась два раза (кликнули double раз)
  })

  it("focus input", () => {
    const {getByTestId} = render(
      <input type="text" data-testid="simple-input" />
    )
    const input = getByTestId("simple-input");
    expect(input).not.toHaveFocus();
    input.focus();
    expect(input).toHaveFocus()
  })

  it("focus", () => {
    const { getAllByTestId } = render(
      <div>
        <input data-testid="element" type="checkbox" />
        <input data-testid="element" type="radio" />
        <input data-testid="element" type="number" />
      </div>
    );
    const [checkbox, radio, number] = getAllByTestId("element");
    userEvent.tab();
    expect(checkbox).toHaveFocus();
    userEvent.tab();
    expect(radio).toHaveFocus();
    userEvent.tab();
    expect(number).toHaveFocus();
  });

  it("select option", () => {
    const { selectOptions, getByRole, getByText } = render(
      <select>
        <option value="1">A</option>
        <option value="2">B</option>
        <option value="3">C</option>
      </select>
    );

    userEvent.selectOptions(getByRole('combobox'), "1");
    expect(getByText("A").selected).toBeTruthy();

    userEvent.selectOptions(getByRole('combobox'), "2");
    expect(getByText("B").selected).toBeTruthy();
    expect(getByText("A").selected).toBeFalsy();
  });
})
