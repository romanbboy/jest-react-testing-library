import React, { useState, useEffect } from "react";
import logo from "./logo.svg";
import "./App.css";

const getPromUser = () => Promise.resolve({id: 1, name: "Roman"})

const Search = ({ value, onChange, children }) => (
  <div>
    <label htmlFor="search">{children}</label>
    <input id="search" type="text" value={value} onChange={onChange} />
  </div>
);

const App = () => {
  const [search, setSearch] = useState("");
  const [user, setUser] = useState(null)

  useEffect(() => {
    const loadUser = async () => {
      const user = await getPromUser();
      setUser(user)
    }

    loadUser();
  }, [])

  const handleChange = ({ target }) => {
    setSearch(target.value);
  };

  return (
    <div>
      {user && <h2>It is: {user.name}</h2>}

      <img className="imageCl xxx" alt="alt img text"/>
      <Search value={search} onChange={handleChange}>
        Search: 777
      </Search>
      <p>Searches for {search ? search : "..."}</p>
    </div>
  );
};

export default App;